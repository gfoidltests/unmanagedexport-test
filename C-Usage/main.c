#include <stdio.h>
#include "MyMath.h"

int main()
{
	int a, b, c;

	printf("a -> ");
	scanf_s("%d", &a);

	printf("b -> ");
	scanf_s("%d", &b);

	c = add(a, b);

	printf("%d + %d = %d\n", a, b, c);

	printf("a = %d\tb = %d\n", a, b);
	swap(&a, &b);
	printf("a = %d\tb = %d\n", a, b);
}