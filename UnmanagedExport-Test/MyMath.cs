﻿using System.Runtime.InteropServices;
using RGiesecke.DllExport;

namespace UnmanagedExport_Test
{
	public static class MyMath
	{
		[DllExport("add", CallingConvention = CallingConvention.Cdecl)]
		public static int Add(int a, int b)
		{
			return a + b;
		}

		[DllExport("swap", CallingConvention = CallingConvention.Cdecl)]
		public static void Swap(ref int a, ref int b)
		{
			int c = a;
			a = b;
			b = c;
		}
	}
}